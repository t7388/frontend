import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Todo } from './todo';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  public appName = 'ToDo List'

  public todos: Todo[] = [];

  public editTodo: Todo | undefined;

  public deleteTodo: Todo | undefined;

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    this.findAllTodos();
  }

  public findAllTodos(): void {
    this.todoService.findAllToDos().subscribe(
      (response: Todo[]) => {
        this.todos = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onOpenModal(todo: Todo | null, mode: string): void {
    const container = document.getElementById('todo-table');
    const button = document.createElement('button');
    button.style.display = 'none';
    button.setAttribute('data-bs-toggle', 'modal');

    if (mode === 'add') {
      button.setAttribute('data-bs-target', '#addTodoModal');
    }
    if (mode === 'edit' && todo !== null) {
      this.editTodo = todo;
      button.setAttribute('data-bs-target', '#editTodoModal');
    }
    if (mode === 'delete' && todo !== null) {
      this.deleteTodo = todo;
      button.setAttribute('data-bs-target', '#deleteTodoModal');
    }

    container?.appendChild(button);
    button.click();
    container?.removeChild(button);
  }

  public onAddTodo(todo: Todo): void {

    this.todoService.addTodo(todo).subscribe(
      (response: Todo) => {
        console.log('Added todo: ' + todo);
        this.findAllTodos();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );

    const button = document.getElementById('btn-close-add-todo');
    button?.click();

  }

  public onEditTodo(todo: Todo): void {

    this.todoService.updateTodo(todo).subscribe(
      (response: Todo) => {
        console.log('Updated todo: ' + todo);
        this.findAllTodos();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );

  }

  public onDeleteTodo(id: number | undefined): void {
    if (id !== undefined) {

      this.todoService.deleteTodo(id).subscribe(
        (response: void) => {
          console.log('Deleted todo with id: ' + id);
          this.findAllTodos();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
      
      const button = document.getElementById('btn-close-delete-todo');
      button?.click();

    }

  }

}
